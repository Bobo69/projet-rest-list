<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ProductListController extends Controller
{
    /**
     * @Route("/product/list", name="product_list")
     */
    public function index()
    {
        return $this->render('product_list/index.html.twig', [
            'controller_name' => 'ProductListController',
        ]);
    }
}
